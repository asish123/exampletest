import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllclassifiedComponent } from './allclassified.component';

describe('AllclassifiedComponent', () => {
  let component: AllclassifiedComponent;
  let fixture: ComponentFixture<AllclassifiedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllclassifiedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllclassifiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
