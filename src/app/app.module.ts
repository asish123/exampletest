import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import { RouterModule, Routes } from '@angular/router';
import { ClassifyComponent } from './classify/classify.component';
import { AllclassifiedComponent } from './allclassified/allclassified.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';

import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import { SuccessLoginComponent } from './success-login/success-login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { MatDialogModule } from '@angular/material';
import { DocformComponent } from './docform/docform.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';


const appRoutes: Routes = [
  { path: 'classify', component: DocformComponent },
  { path: 'classified', component: ClassifyComponent},
  { path: 'allclassified', component: AllclassifiedComponent},
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent},
  { path: 'successlogin', component: SuccessLoginComponent},
  { path: 'welcome', component: WelcomeComponent},
  { path: '', redirectTo: '/welcome', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ClassifyComponent,
    AllclassifiedComponent,
    SignupComponent,
    LoginComponent,
    SuccessLoginComponent,
    WelcomeComponent,
    DocformComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    RouterModule.forRoot( appRoutes,{ enableTracing: true }),
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig,'exampletest'),
    FormsModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    HttpClientModule,
    

  ],
  providers: [AngularFireAuth,AngularFirestore],
  bootstrap: [AppComponent],
})
export class AppModule { }
