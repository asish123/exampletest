import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  
  constructor(public afAuth:AngularFireAuth ,private router:Router) { 
    this.user = this.afAuth.authState;
  }
  private logInErrorSubject = new Subject<string>();

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
    

  }

  signup(email:string, passwoerd:string){
        this.afAuth.auth
            .createUserWithEmailAndPassword(email,passwoerd)
            .then(res => console.log('Successful Signup',res))
            .catch (
                        error => window.alert(error))
              this.router.navigate(['/successlogin']);
          //  this.router.navigate(['/successlogin']);  
            
      }
      Logout(){
         this.afAuth.auth.signOut()
        .then(res => console.log('Successful logout',res));
        this.router.navigate(['/welcome']);  
          }

    login(email:string, password:string){
         this.afAuth
          .auth.signInWithEmailAndPassword(email,password)
           .then(res => console.log('Succesful Login',res))
          .catch (
                     error => window.alert(error)
                       ) 
            this.router.navigate(['/successlogin']);        
              }
             
}
