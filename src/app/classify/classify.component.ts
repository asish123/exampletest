import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ImageService } from '../image.service';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string;


  constructor(public classifyService:ClassifyService,
              public imageService:ImageService) {}

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        this.category = this.classifyService.categories[res];

        this.categoryImage = this.imageService.images[res];
  
      }
     ) }
 
     
}
