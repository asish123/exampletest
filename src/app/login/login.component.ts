import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  password:string;
  private errorMessage:string;

    constructor(public auth:AuthService, private router:Router) { 
      this.auth.getLoginErrors().subscribe(error => {
              this.errorMessage = error;
            });
    }
  
    ngOnInit() {
    }
  
    OnSubmitLogin(){
      this.auth.login(this.email,this.password);
    this.router.navigate(['/successlogin']);
    }
  }