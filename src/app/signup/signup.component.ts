import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  email:string;
  password:string;
  private errorMessage:string;

   constructor(private atuh:AuthService,private router:Router) { 
    this.atuh.getLoginErrors().subscribe(error => {
            this.errorMessage = error;    
          
          });
  }
  ngOnInit() {

  }

  OnSubmitSignUp(){
    this.atuh.signup(this.email,this.password);
      // this.router.navigate(['/successlogin']);
  }

}
