// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDxq1ly9yMPqIcd9LK3H87J0o0eiWLsC98",
    authDomain: "exampletest-5693a.firebaseapp.com",
    databaseURL: "https://exampletest-5693a.firebaseio.com",
    projectId: "exampletest-5693a",
    storageBucket: "exampletest-5693a.appspot.com",
    messagingSenderId: "683802674030",
    appId: "1:683802674030:web:d9d93fab780db76e2cb77f",
    measurementId: "G-VLNX06FJXC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
